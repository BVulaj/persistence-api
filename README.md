persistence-api
===============

Provides a generic interface for basic repositories.

Implementations
------
* [persistence-api-hibernate](http://github.com/BVulaj/persistence-api-hibernate)
